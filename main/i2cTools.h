/*
 * i2cTools.h
 *
 *  Created on: 05-10-2020
 *      Author: aleja
 */

#ifndef MAIN_I2CTOOLS_H_
#define MAIN_I2CTOOLS_H_


#define I2C_MASTER_ACK 0
#define I2C_MASTER_NACK 1

#define ICM20948_ADDR 0x68
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */


void i2cMasterInit(void);
void i2cReadData(uint8_t , uint8_t ,uint8_t , uint8_t *);
void i2cSetData(uint8_t , uint8_t, uint8_t, uint8_t *);
void i2cSetOneByte(uint8_t chipAddr, uint8_t registerAddr,  uint8_t data);
uint8_t i2cReadOneByte(uint8_t chipAddr, uint8_t registerAddr);


#endif /* MAIN_I2CTOOLS_H_ */
