/*
 * LTC2487.h
 *
 *  Created on: 07-10-2020
 *      Author: aleja
 */

#ifndef MAIN_LTC2487_H_
#define MAIN_LTC2487_H_
#include "stdbool.h"

//Channel selection
#define LTC24XX_MULTI_CH_CH0            0xB0
#define LTC24XX_MULTI_CH_CH1            0xB8
#define LTC24XX_MULTI_CH_CH2            0xB1
#define LTC24XX_MULTI_CH_CH3            0xB9


#define LTC24XX_MULTI_CH_P0_N1          0xA0
#define LTC24XX_MULTI_CH_P1_N0          0xA8

#define LTC24XX_MULTI_CH_P2_N3          0xA1
#define LTC24XX_MULTI_CH_P3_N2          0xA9


#define LTC24XX_INTERNAL_TEMP           0xC0

#define	LTC24XX_ADDR					0x14


// Select rejection frequency - 50, 55, or 60Hz
#define LTC24XX_EZ_MULTI_R50    0b10010000
#define LTC24XX_EZ_MULTI_R55    0b10000000
#define LTC24XX_EZ_MULTI_R60    0b10100000

#define LTC24XX_vref 3.3f
float LTC2487ReadTemperature(void);
bool checkLTCPrecense(void);
typedef union
{
  int32_t LT_int32;    //!< 32-bit signed integer to be converted to four bytes
  uint32_t LT_uint32;  //!< 32-bit unsigned integer to be converted to four bytes
  uint8_t LT_byte[4];  //!< 4 bytes (unsigned 8-bit integers) to be converted to a 32-bit signed or unsigned integer
}LT_union_int32_4bytes;
#endif /* MAIN_LTC2487_H_ */
