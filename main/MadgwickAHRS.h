/*
 * MadgwickAHRS.h
 *
 *  Created on: 06-10-2020
 *      Author: aleja
 */

#ifndef MAIN_MADGWICKAHRS_H_
#define MAIN_MADGWICKAHRS_H_

#include "math.h"
#include "ICM20948.h"

float getRoll(void);
float getPitch(void);
float getYaw(void);
void updateIMU(ICM_20948_AGMT_t data);

#endif /* MAIN_MADGWICKAHRS_H_ */
