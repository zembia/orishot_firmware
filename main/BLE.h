/*
 * BLE.h
 *
 *  Created on: 09-10-2020
 *      Author: aleja
 */

#ifndef MAIN_BLE_H_
#define MAIN_BLE_H_
#include "ICM20948.h"
#include "MadgwickAHRS.h"
void ble_init(void);

#endif /* MAIN_BLE_H_ */
